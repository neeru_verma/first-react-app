// import logo from './logo.svg';
import './App.css';
import Header from './Header';
// import ReactFullpage from './ReactFullpage';
// import ReactSwiper from './ReactSwiper';
// import CarouselSlider from './CarouselSlider';
// import Clock from './Clock';
// import Footer from './Footer';
// import Part1 from './Part1';
import Home from './Home';
import Footer from './Footer';
import Contact from './Contact';
import About from './About';
import Service from './Service';
import LearnClassComponent from './LearnClassComponent';

import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  NavLink
} from 'react-router-dom';

function App() {
  return (
    <div>
      <Header/>
      {/* <ReactFullpage /> */}
      {/* <ReactSwiper /> */}
      {/* <Clock /> */}
      {/* <Part1 /> */}
      {/* <CarouselSlider /> */}
      <div className='body'>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route exact path="/about" element={<About />}></Route>
        <Route exact path="/service" element={<Service />}></Route>
        <Route exact path="/contact" element={<Contact />}></Route>
      </Routes>
      </div>
      <LearnClassComponent />
                
      <Footer />
    </div>



//     <Router>
//     <div className="App">
//     <Header/>
//     </div>
// </Router>
  );
}

export default App;
