import React, { Component } from 'react';
// var ReactDOM = require('react-dom/client');
class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

  componentDidMount() {
    console.warn('componentDidMount');
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    console.warn('componentWillUnmount');
    clearInterval(this.timerID);
  }

  tick() {
    console.warn('tick');
    
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <p className='clock'>Clock : {this.state.date.toLocaleTimeString()}.</p>
      </div>
    );
  }
}

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(<Clock />);


export default Clock;