import React, { Component } from 'react'
// import Footer from './Footer';
import Banner from './banner.jpg';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
export default class Contact extends Component {
  render() {
    return (
      <div>
        <img src={Banner} alt="banner" class="banner" />
        <div className='bheading'><h1 class="text-center ">Contact Us</h1></div>

        <div className="content">
          <div className='row'>
            <div className='col-md-10 offset-1'>
              <div className='row'>
                <div className='col-md-6'>
                  {/* <iframe src="https://goo.gl/maps/FDb7ZSTUReruNPCd9"></iframe> */}
                  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d56149.06477207521!2d77.0814377!3d28.4097038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1671041681665!5m2!1sen!2sin"
                    width="100%" height="450" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div className='col-md-6 '>
                  <div className='forms'>
                    <Form>
                      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="name@example.com" />
                      </Form.Group>
                      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Message</Form.Label>
                        <Form.Control as="textarea" rows={3} />
                      </Form.Group>
                      <Button type="button">Submit</Button>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        {/* <Footer /> */}
      </div>
    )
  }
}
