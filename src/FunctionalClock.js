// import React from 'react';
// import ReactDOM from 'react-dom';
// const rot = ReactDOM.createRoot(
//   document.getElementById('rot')
// );

// function FunctionalClock(props) {
//   return (
//     <div>
//       <h2>Functional Clock : {props.date.toLocaleTimeString()}.</h2>
//     </div>
//   );
// }

// function tick() {
//   return <FunctionalClock date={new Date()} />;
// }

// setInterval(tick, 1000);

// export default FunctionalClock;
import React, { useEffect } from 'react'

export default function FunctionalClock() {
  useEffect(() => {
    setInterval(tick, 1000);
  });
  return <tick date={new Date()} />;
}

function tick(props) {
  var date = new Date();
  return (
    <div>
      <h2>Functional Clock : {date.toLocaleTimeString()}.</h2>
    </div>
  );
}
