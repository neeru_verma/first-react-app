import React, { Component } from 'react'
// import ReactFullpage from './ReactFullpage';
import ReactSwiper from './ReactSwiper';
import CarouselSlider from './CarouselSlider';
import Clock from './Clock';
import Footer from './Footer';
import Part1 from './Part1';
import LearnClassComponent from './LearnClassComponent';
export default class Home extends Component {
    render() {
        return (
            <div>
                {/* <Header /> */}
                {/* <ReactFullpage /> */}
                <ReactSwiper />
                <Clock />
                <Part1 />
                <CarouselSlider />
                {/* <Footer /> */}
            </div>
        )
    }
}
