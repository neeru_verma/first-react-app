import React, { Component } from 'react'

export default class LearnClassComponent extends Component {
    constructor(){
        super();
        this.state={
            data:1
        }
    }
    increment(){
      if(this.state.data<5)
        this.setState({data:this.state.data+1})
    }
    decrement(){
        if(this.state.data>1)
        this.setState({data:this.state.data-1})
    }

  render() {
    console.log('render')
    return (
      <div>
        {/* <h5>Quantity : </h5> */}
        <div class="row feedback">
        <div class="col-md-6 offset-3">
          <div className='row'>
            <div className='col-md-6 col-xs-12'><p>Please Rate Us Out of 5: </p></div>
            <div className='col-md-6 col-xs-12'> <p>
          <div class="row">
            <div class="col-md-1 col-xs-4"><button onClick={()=>this.increment()}>+</button></div>
            <div class="col-md-3 col-xs-4"><input value={this.state.data} class="form-control" /></div>
            <div class="col-md-1 col-xs-4"><button onClick={()=>this.decrement()}>-</button></div>
        </div> 
          </p>
        </div>
          </div>
          
         
        {/* <div class="row">
            <div class="col-md-1"><button onClick={()=>this.increment()}>+</button></div>
            <div class="col-md-10"><input value={this.state.data} class="form-control" /></div>
            <div class="col-md-1"><button onClick={()=>this.decrement()}>-</button></div>
        </div> */}
        </div>
        </div>
        
      </div>
    )
  }
}
