// import logo from './logo.svg';
import './App.css';
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
function Logo(props) {
    return (
        // <Navbar.Brand href="#home"><img src={logo}  className="App-logo" alt="logo" /></Navbar.Brand>
        <Navbar.Brand href="#home"><img src={props.data.img}  className={props.data.name} alt="logo" /></Navbar.Brand>
        // <Navbar.Brand href="#home">Logo</Navbar.Brand>
    );
}

export default Logo;
