// import logo from './logo.svg';
import logo from './n1.png';
import './App.css';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import Logo from './Logo';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {
    // BrowserRouter as Router,
    // Routes,
    // Route,
    Link
} from 'react-router-dom';

function Navigation(props) {
    return (

        <div class="navigations">
          
{/* <Navbar bg="light" expand="lg" id="nav"> */}
<Navbar bg="light" expand="lg" >
<Container class="links">
    {/* <Logo img={logo} name={props.name} /> */}
    <Logo data = {{name:props.name,img:logo}} />
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
        <Link to="/">Home</Link>&nbsp; &nbsp; 
   <Link to="/about">About</Link> &nbsp; &nbsp;
   <Link to="/service">Services</Link> &nbsp; &nbsp;
   <Link to="#">Packages</Link> &nbsp; &nbsp;
   <Link to="/contact">Contact</Link>&nbsp;
            {/* <Link to="/"><Nav.Link>Home</Nav.Link></Link>
            <Link to="/about" ><Nav.Link>About Us</Nav.Link></Link>
            <Link to="/Contact" ><Nav.Link>Contact Us</Nav.Link></Link> */}
            {/* <Nav.Link href="/Contact">Contact Us</Nav.Link> */}
            {/* <NavDropdown title="My Account" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                    My Account
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Dashboard</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                    Settings
                </NavDropdown.Item>
            </NavDropdown> */}
        </Nav>
        <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
    </Navbar.Collapse>
</Container>
</Navbar>


   {/* <Link to="/">Home</Link> 
   <Link to="/about">About</Link> 
   <Link to="/contact">Contact</Link>  */}
            {/* <Routes>
                 <Route exact path='/' element={< Home />}></Route>
                 <Route exact path='/About' element={< About />}></Route>
                 <Route exact path='/Contact' element={< Contact />}></Route>
          </Routes> */}


        </div>


    );
}

export default Navigation;
