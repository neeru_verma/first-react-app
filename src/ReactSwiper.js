import React from 'react'
// import { Swiper, SwiperSlide } from 'swiper/react';

// // Import Swiper styles
// import 'swiper/css';
// import Swiper core and required modules
// import Swiper core and required modules
import { Autoplay, Navigation, Pagination } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
// import 'swiper/css/scrollbar';
import slide1 from './slide1.jpg';
import slider6 from './slider6.jpeg';
import slider1 from './slider1.jpg';
// import slider2 from './slider2.jpeg';
import slider2 from './slider2.jpg';
// import slider3 from './slider3.jpg';
import slider4 from './slider4.jpg';
import slider5 from './slider5.jpg';
export default function ReactSwiper() {
  return (
    <div>
      <Swiper
      // install Swiper modules
      spaceBetween={30}
      centeredSlides={true}
      autoplay={{
        delay: 2500,
        disableOnInteraction: false,
      }}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[Autoplay, Pagination, Navigation]}
      className="mySwiper"
    >
      <SwiperSlide><img class="slider" src={slider2}  alt="" /></SwiperSlide>
      <SwiperSlide><img class="slider" src={slide1}  alt="" /></SwiperSlide>
      <SwiperSlide><img class="slider" src={slider1}  alt="" /></SwiperSlide>
      {/* <SwiperSlide><img class="slider" src={slider6}  alt="" /></SwiperSlide> */}
      {/* <SwiperSlide><img class="slider" src={slider3}  alt="" /></SwiperSlide> */}
      {/* <SwiperSlide><img class="slider" src={slider4}  alt="" /></SwiperSlide> */}
      {/* <SwiperSlide><img class="slider" src={slider5}  alt="" /></SwiperSlide> */}
      ...
    </Swiper>
    </div>
  )
}
