import React, { Component } from 'react'
import Banner from './banner.jpg';
export default class Service extends Component {
  render() {
    return (
      <div>
        <img src={Banner} alt="banner" class="banner" />
        <div className='bheading'><h1 class="text-center ">Services</h1></div>

        <div className="content">
         <p>There are 4 services : </p>
         <ul>
          <li>Angular</li>
          <li>React</li>
          <li>Vue</li>
          <li>Laravel</li>

         </ul>
       </div>
        {/* </div> */}
        {/* <Footer /> */}
      </div>

    )
  }
}
