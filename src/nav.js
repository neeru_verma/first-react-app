  
<Navbar bg="light" expand="lg">
<Container>
    {/* <Logo img={logo} name={props.name} /> */}
    <Logo data = {{name:props.name,img:logo}} />
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
            <Link to="/"><Nav.Link>Home</Nav.Link></Link>
            <Link to="/About" ><Nav.Link>About Us</Nav.Link></Link>
            <Link to="/Contact" ><Nav.Link>Contact Us</Nav.Link></Link>
            {/* <Nav.Link href="/Contact">Contact Us</Nav.Link> */}
            <NavDropdown title="My Account" id="basic-nav-dropdown">
                {/* <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item> */}
                <NavDropdown.Item href="#action/3.2">
                    My Account
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Dashboard</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                    Settings
                </NavDropdown.Item>
            </NavDropdown>
        </Nav>
    </Navbar.Collapse>
</Container>
</Navbar>

